from django.db import models
from seller.models import SellerModel

class Customer(models.Model):

	item = models.ForeignKey(SellerModel, on_delete=models.CASCADE, related_name="bids")
	name = models.CharField(max_length=100, blank=False)
	price = models.PositiveIntegerField(default=0)
	address =  models.CharField(max_length=10000, blank=False)
	email =  models.EmailField(max_length=254, blank=False)
	phone_number =  models.CharField(max_length=20, blank=False)
	terms_confirm =  models.BooleanField(blank=False)
	
	def __str__(self):
		return str(self.price)