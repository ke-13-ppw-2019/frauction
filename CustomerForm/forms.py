from django import forms
from .models import Customer
from django.forms import ModelForm

class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ['name','price','address','email','phone_number','terms_confirm']
        labels = {'name':'Name','price':'Price','address':'Address','email':'E-mail','phone_number':'Phone Number','terms_confirm':'I have agreed to the terms and condition'}
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'price':forms.NumberInput(attrs={'class':'form-control','type':'number'}),
            'address':forms.Textarea(attrs={'class':'form-control','type':'textarea','row':'10'}),
            'email':forms.EmailInput(attrs={'class':'form-control','type':'email'}),
            'phone_number':forms.TextInput(attrs={'class':'form-control','type':'text'}),
            'terms_confirm':forms.CheckboxInput(attrs={'class':'form-control','type':'checkbox'}),
            }