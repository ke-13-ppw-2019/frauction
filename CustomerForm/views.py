from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from .forms import CustomerForm
from .models import Customer
from django.shortcuts import redirect
from .models import SellerModel

def bid_form(request, id):
	item = SellerModel.objects.get(id=id)
	context = {
		'item' : item,
		'form' : CustomerForm(),
		'title' : 'Place Bid',
		'navlink' : reverse('seller_create'),
		'sellorbuy' : 'Sell',
		}
			
	return render(request, 'BuyerForm.html', context)	

def place_bid(request, id):
	if request.method == "POST":
		form = CustomerForm(request.POST)
		if form.is_valid():
			tc = False
			if request.POST.get('terms_confirm') == 'on':
				tc = True
			
			Customer.objects.create(
				item = SellerModel.objects.get(id=id),
				name = request.POST.get('name'),
				price = request.POST.get('price'),
				address = request.POST.get('address'),
				email = request.POST.get('email'),
				phone_number = request.POST.get('phone_number'),
				terms_confirm = tc,)
			
			lastCustomer = Customer.objects.last()
			context = {
				'title' : 'Place Bid',
				'last' : lastCustomer,
				'navlink' : reverse('seller_create'),
				'sellorbuy' : 'Sell',
			}
			return render(request,'thanks_bid.html', context)	
			
		
		
		
		
		
		
		