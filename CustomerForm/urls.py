from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'CustomerForm'

urlpatterns = [
	path('<int:id>', views.bid_form, name='bid'),
    path('thanks/<int:id>', views.place_bid, name='place_bid'),
	]