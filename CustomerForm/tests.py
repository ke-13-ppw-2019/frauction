from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import Customer
from .forms import CustomerForm

# Create your tests here.
class Testing(TestCase):
	def test_apakah_url_CustomerForm_ada(self):
		response = Client().get('/CustomerForm')
		self.assertEqual(response.status_code, 404)
		
	def test_apakah_jika_memasukkan_url_lain_maka_url_tidak_ada(self):
		response = Client().get('/notexist/')
		self.assertEqual(response.status_code, 404)
		
	# def test_apakah_buyerform_html_ditampilkan(self):
		# response = Client().get('/CustomerForm/<int:id>')
		# self.assertTemplateUsed(response, 'BuyerForm.html')
		
	# def test_apakah_terdapat_fungsi_bid_form(self):
		# found = resolve('/CustomerForm <int:id>')
		# self.assertEqual(found.func, bid_form)
		
	# def test_apakah_terdapat_fungsi_place_bid(self):
		# found = resolve('/CustomerForm/thanks/<int:id>')
		# self.assertEqual(found.func, place_bid)
		
	# def test_apakah_terdapat_tulisan_PleaseFillOutThisInformation(self):
		# request = HttpRequest()
		# response = CustomerForm(request)
		# html_response = response.content.decode('utf8')
		# self.assertIn('Please Fill Out This Information', html_response)