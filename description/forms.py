from django import forms

qn_attrs = {
        'class': 'form-control form-group',
        'placeholder': 'message/question',
        }

ans_attrs = {
        'class': 'form-control form-group',
        'placeholder': 'Write your reply',
        }

class QuestionForm(forms.Form):
    question = forms.CharField(label="", required=True, max_length=400, widget=forms.TextInput(attrs=qn_attrs))

class AnswerForm(forms.Form):
    answer = forms.CharField(label="", required=True, max_length=400, widget=forms.TextInput(attrs=ans_attrs))
