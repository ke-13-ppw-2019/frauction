from django.test import TestCase, Client
from .models import Question, Answer, SellerModel
import datetime

class DescriptionUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        SellerModel.objects.create(
            id = 1, name = "wulan", address = "fasilkom ui", phone_number = "081234567890",
            email = "wulan.mantiri@gmail.com", bank_name = "bca", bank_account = "12345678",
            title = "chevrolet", start_date = datetime.datetime.now(), end_date = "2019-12-10 17:00:00+00:00",
            starting_price = "15", price="15", image = "assets/ferrari.jpg", description = "good", agree = True  )
        SellerModel.objects.create(
            id = 2, name = "wulan", address = "fasilkom ui", phone_number = "081234567890",
            email = "wulan.mantiri@gmail.com", bank_name = "bca", bank_account = "12345678",
            title = "avanza", start_date = datetime.datetime.now(), end_date = "2018-12-10 17:00:00+00:00",
            starting_price = "25", price="25", image = "assets/ferrari.jpg", description = "", agree = True  )
        self.desc_page_content = self.client.get('/1/chevrolet').content.decode('utf8')
        self.avanza_page_content = self.client.get('/2/avanza').content.decode('utf8')

    def test_url_exists(self):
        response = self.client.get('/1/chevrolet')
        self.assertEqual(response.status_code, 200)
    
    def test_url_exists_two(self):
        response = self.client.get('/2/avanza')
        self.assertEqual(response.status_code, 200)

    def test_create_new_question_model(self):
        Question.objects.create(item = SellerModel.objects.get(id=1),
            question = "Can I create a new question?",
            time = datetime.datetime.now() )
        self.assertEqual(len(Question.objects.all()), 1)
    
    def test_save_POST_request_when_question_field_left_blank(self):
        response = self.client.post('/ask/1', 
            data= {'item': SellerModel.objects.get(id=1), 
                'question': "", 
                'time': datetime.datetime.now() })
        self.assertEqual(len(Question.objects.all()), 0)
        self.assertEqual(response.status_code, 302)

    def test_save_POST_request_when_question_field_is_filled(self):
        response = self.client.post('/ask/1',
            data= {'item': SellerModel.objects.get(id=1), 
                'question': "Can I ask question by POST request?", 
                'time': datetime.datetime.now() })
        self.assertEqual(len(Question.objects.all()), 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/1/chevrolet')

    def test_create_new_answer_model(self):
        Question.objects.create(id=1, item = SellerModel.objects.get(id=1),
            question = "Can I create a new question?",
            time = datetime.datetime.now() )
        Answer.objects.create(
            question = Question.objects.get(id=1),
            answer = "Yay, I can make questions. Now, can I answer them?",
            time = datetime.datetime.now() )
        self.assertEqual(len(Answer.objects.all()), 1)

    def test_save_POST_request_when_answer_field_left_blank(self):
        Question.objects.create(id=1, item = SellerModel.objects.get(id=1),
            question = "Can I create a new question?",
            time = datetime.datetime.now() )
        response = self.client.post('/answer/1/1',
            data= {
                'question': Question.objects.get(id=1), 
                'answer': "", 
                'time': datetime.datetime.now() })
        self.assertEqual(len(Answer.objects.all()), 0)
        self.assertEqual(response.status_code, 302)

    def test_save_POST_request_when_answer_field_is_filled(self):
        Question.objects.create(id=1, item = SellerModel.objects.get(id=1),
            question = "Can I create a new question?",
            time = datetime.datetime.now() )
        response = self.client.post('/answer/1/1',
            data= {'question': Question.objects.get(id=1), 
                'answer': "Now, can I reply the questions by answering through POST request?", 
                'time': datetime.datetime.now() })
        self.assertEqual(len(Answer.objects.all()), 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/1/chevrolet')

    def test_if_item_title_exists(self):
        self.assertIn("chevrolet", self.desc_page_content)

    def test_if_item_image_exists(self):
        self.assertIn("<img", self.desc_page_content)
        self.assertIn("/media/assets/ferrari.jpg", self.desc_page_content)

    def test_if_primary_bidding_information_exists(self):
        self.assertIn("This Auction Ends in:", self.desc_page_content)

    def test_if_bidding_time_is_not_over(self):
        self.assertIn("days", self.desc_page_content)
        self.assertIn("Current Price:", self.desc_page_content)

    def test_if_button_place_bid_exists(self):
        self.assertIn("<a href=\"/CustomerForm/1\"", self.desc_page_content)
        self.assertIn("Place Bid", self.desc_page_content)
    
    def test_if_bidding_time_is_over(self):
        self.assertIn("Time&#39;s Up", self.avanza_page_content)
        self.assertIn("Final Price:", self.avanza_page_content)
        self.assertIn("onclick=\"return false\"", self.avanza_page_content)

    def test_if_information_part_exists(self):
        self.assertIn("Information", self.desc_page_content)
        self.assertIn("Auction Details", self.desc_page_content)
        self.assertIn("Payment", self.desc_page_content)
        self.assertIn("Item Description", self.desc_page_content)
        self.assertIn("Additionals", self.desc_page_content)

    def test_if_auction_details_exist(self):
        self.assertIn("Starting Bid:", self.desc_page_content)
        self.assertIn("Current Bid:", self.desc_page_content)
        self.assertIn("Start Date:", self.desc_page_content)
        self.assertIn("End Date:", self.desc_page_content)
        self.assertIn("Bid Count:", self.desc_page_content)

    def test_if_question_input_exists(self):
        self.assertIn("QnA", self.desc_page_content)
        self.assertIn("<input type=\"text\" name=\"question\"", self.desc_page_content)
        self.assertIn("<button type=\"submit\"", self.desc_page_content)