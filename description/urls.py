from django.conf.urls import url
from django.urls import path
from .views import desc, ask_question, answer_question

app_name = 'description'

urlpatterns = [
    path('<int:id>/<str:title>', desc, name="desc"),
    path('ask/<int:id>', ask_question, name="ask"),
    path('answer/<int:item_id>/<int:id>', answer_question, name="ans"),
]