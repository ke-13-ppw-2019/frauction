from django.db import models
from seller.models import SellerModel

class Question(models.Model):
	item = models.ForeignKey(SellerModel, on_delete=models.CASCADE, related_name="questions")
	question = models.CharField(max_length=400, blank=False)
	time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.question

class Answer(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="answers")
	answer = models.CharField(max_length=400, blank=False)
	time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.answer