from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Question, Answer, SellerModel
from .forms import QuestionForm, AnswerForm
import datetime

def desc(request, id, title):

	item = SellerModel.objects.get(id=id)
	enddate = datetime.datetime(item.end_date.year, item.end_date.month, item.end_date.day, item.end_date.hour)
	daysleft = (enddate - datetime.datetime.now()).days
	hoursleft = (enddate - datetime.datetime.now()).seconds//3600
	countdown = str(daysleft) + " days "
	curorlast = "Current"
	onclick = ""

	if hoursleft == 1:
		countdown += str(hoursleft) + " hour"
	elif hoursleft > 1:
		countdown += str(hoursleft) + " hours"
	
	if daysleft < 0:
		countdown = "Time's Up"
		curorlast = "Final"
		onclick = "return false"

	lastbid = item.bids.last()
	lastprice = lastbid
	if lastbid == None:
		lastbid = 0
		lastprice = item.starting_price
	
	context = {
		'qnform': QuestionForm(),
		'ansform' : AnswerForm(),
        'title': "Frauction: " + item.title,
		'navlink' : reverse('seller_create'),
		'sellorbuy' : 'Sell',
		'item' : item,
		'countdown' : countdown,
		'oc' : onclick,
		'curorlast' : curorlast,
		'lastbid' : lastbid,
		'lastprice' : lastprice,
		'bidcount' : item.bids.count,
    }
	return render(request, 'description_page.html', context)

def ask_question(request, id):

	item = SellerModel.objects.get(id=id)
	if request.method == "POST":
		form = QuestionForm(request.POST)
		if form.is_valid():
			Question.objects.create(
				item = item,
				question = request.POST.get('question'),
				time = request.POST.get('time'),
			)

	return redirect('description:desc', id, item.title)

def answer_question(request, id, item_id):

	item = SellerModel.objects.get(id=item_id)
	if request.method == "POST":
		form = AnswerForm(request.POST)
		if form.is_valid():
			Answer.objects.create(
				question = Question.objects.get(id=id),
				answer = request.POST.get('answer'),
				time = request.POST.get('time'),
			)
	else:
		form = AnswerForm()
	return redirect('description:desc', item_id, item.title)