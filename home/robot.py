from seller.models import SellerModel
from CustomerForm.models import Customer
import datetime

def update_price_robot():
    for obj in SellerModel.objects.all():
        enddate = datetime.datetime(obj.end_date.year,obj.end_date.month, obj.end_date.day, obj.end_date.hour)
        daysleft = (enddate - datetime.datetime.now()).days
        hoursleft = (enddate - datetime.datetime.now()).seconds//3600
        lastbid = obj.bids.last()
        if ((lastbid == None) and (obj.price==0)):
            obj.price = obj.starting_price
        elif (lastbid != None) and (lastbid.price > obj.price):
            obj.price = lastbid.price

        if(daysleft<0):
            obj.status = 'Sold out'
            obj.save()
            continue

        obj.price = obj.price + 5
        obj.save()

        Customer.objects.create(
            item=obj, 
            name="Robot", 
            price=obj.price, 
            address="Dunia lain", 
            phone_number=4040404, 
            terms_confirm=True)