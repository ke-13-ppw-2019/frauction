from django import forms

attrs = {
    'class': 'form-control form-group' 
}

sort_by_choices = (
    ('Newest',"Newest"),
    ('Oldest', "Oldest"),
    ('Alphabetical',"Alphabetical"),
    ('Price (from lowest)',"Price (from lowest)"),
    ('Price (from highest)',"Price (from highest)"),
    ('End Date (from nearest)',"End Date (from nearest)"),
    ('End Date (from farthest)',"End Date (from farthest)")
)

class SortByForm(forms.Form):
    sort_by =  forms.ChoiceField(label='Sort by',widget=forms.Select(attrs=attrs),choices=sort_by_choices, initial="Newest")