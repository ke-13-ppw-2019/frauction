from . import views
from django.urls import path

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    #path('sort_by',views.sort_by_choice,name='sort_by')
]