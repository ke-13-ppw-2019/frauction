from django.shortcuts import render, redirect
from .forms import SortByForm
from seller.models import SellerModel
from .models import SortBy
from CustomerForm.models import Customer
from django.urls import reverse
from . import robot

# Create your views here.
def home(request):
    if request.method == 'POST':
        form = SortByForm(request.POST)
        if form.is_valid():
            this_choice = request.POST.get('sort_by')
            create_object(this_choice)
            
            lastSortObject = SortBy.objects.last()
            get_objects = get_objects_sorted_by(this_choice)
           
            response = {
                'title' : 'Frauction',
                'form' : form,
                'seller' : get_objects_sorted_by(this_choice),
                'last' : lastSortObject,
                'sellorbuy' : 'Sell',
                'navlink' : reverse('seller_create')
            }
            return render(request,("home.html"),response)
    else:
        form = SortByForm()
        create_object('Newest')
        lastSortObject = SortBy.objects.last()

        robot.update_price_robot()

    response = {
        'title' : 'Frauction',
        'form' : form,
        'seller' : get_objects_sorted_by('Newest'),  
        'last' : lastSortObject,
        'sellorbuy' : 'Sell',
        'navlink' : reverse('seller_create'),
    }
    return render(request,("home.html"),response)

def create_object(curr_choice):
    sortbyobjects = SortBy(sort_by_choice=curr_choice)
    sortbyobjects.save()

def get_objects_sorted_by(choice):
    if(choice=='Newest'):
        return SellerModel.objects.all().order_by('-start_date')
    elif(choice=='Oldest'):
        return SellerModel.objects.all().order_by('start_date')
    elif(choice=='Alphabetical'):
        return SellerModel.objects.all().order_by('title')
    elif(choice=='Price (from lowest)'):
        return SellerModel.objects.all().order_by("price")
    elif(choice=='Price (from highest)'):
        return SellerModel.objects.all().order_by("-price")
    elif(choice=='End Date (from nearest)'):
        return SellerModel.objects.all().order_by("end_date")
    elif(choice=='End Date (from farthest)'):
        return SellerModel.objects.all().order_by('-end_date')
