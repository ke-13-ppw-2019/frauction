from django.db import models

# Create your models here.
class SortBy(models.Model):

    class Meta:
        verbose_name_plural = 'Sort-by(s)'

    sort_by_choice = models.CharField(max_length=30)
    lastSort = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sort_by_choice