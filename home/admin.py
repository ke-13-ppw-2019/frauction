from django.contrib import admin
from .models import SortBy

# Register your models here.
admin.site.register(SortBy)