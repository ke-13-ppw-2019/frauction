from django.test import TestCase, Client
from .models import SortBy
from seller.models import SellerModel
from .forms import SortByForm
from CustomerForm.models import Customer
from . import robot

# Create your tests here.
class SortByModelTest(TestCase):

    def test_string_representation(self):
        entry = SortBy(sort_by_choice='choice')
        self.assertEqual(str(entry),entry.sort_by_choice)

    def test_verbose_name_plural(self):
        self.assertEqual(str(SortBy._meta.verbose_name_plural), "Sort-by(s)")

class HomePageTest(TestCase):

    def test_if_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_if_homepage_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_if_goods_is_empty(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        seller = SellerModel.objects.all()
        if(len(seller)==0):
            self.assertIn("Place your first items here!",content)

    def test_if_goods_exists(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-12-10 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 10
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        response = Client().get('/')
        content = response.content.decode('utf8')
        hitungjumlah = SellerModel.objects.all().count()
        self.assertEqual(hitungjumlah,1)
        self.assertIn('flex-wrap',content)
        self.assertIn('card-barang',content)
        """
        if(SellerModel.objects.all().count):
            self.assertIn('flex-wrap', content)
            self.assertIn('card-barang',content)
        """

    def test_if_goods_properties_exists(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-12-10 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 10
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        response = Client().get('/')
        content = response.content.decode('utf8')
        
        obj = SellerModel.objects.get(id=1)
        self.assertIn(obj.title, content)
        self.assertIn(str(obj.price), content)
        self.assertIn(obj.status, content)

    def test_is_sort_by_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Sort by',content)
        self.assertIn('<option value="Newest"',content)
        self.assertIn('<option value="Oldest"',content)
        self.assertIn('<option value="Alphabetical"',content)
        self.assertIn('<option value="Price (from lowest)"',content)
        self.assertIn('<option value="Price (from highest)"',content)
        self.assertIn('<option value="End Date (from nearest)"', content)
        self.assertIn('<option value="End Date (from farthest)"', content)
        self.assertIn('<button class="sort-button"', content)

    def test_is_navbar_and_the_contents_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('nav',content)
        self.assertIn('Home',content)
        self.assertIn('Want to Sell?',content)
        self.assertIn('frauction-logo.png',content)

    def test_is_last_sorted_by_exists(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-12-10 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 10
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Last sorted by',content)

    def test_if_intro_background_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('logo-horizontal.png',content)
        self.assertIn('Best place to bid.',content)

    def test_if_robot_works_without_last_bid(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-12-10 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 0
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        response = Client().get('/')
        obj = SellerModel.objects.get(id=1)
        #robot.update_price_robot()
        content = response.content.decode('utf8')
        self.assertIn(str(obj.starting_price+5),content)

    def test_if_robot_works_with_last_bid(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-12-10 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 0
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        obj = SellerModel.objects.get(id=1)
        Customer.objects.create(
            item = obj,
            name = 'Robot',
            price = 37,
            address = 'ADDRESS',
            email = 'email@email.com',
            phone_number = 17492,
            terms_confirm=True
        )
        customer = Customer.objects.get(id=1)
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn(str(customer.price+5),content)

    def test_if_robot_works_sold_out(self):
        name = 'test'
        address = 'dimana'
        phone_number = 923
        email = 'gjf@gmail.com'
        bank_name = 'bni'
        bank_account = '39223'
        title = 'barang'
        end_date = "2019-05-05 17:00:00+00:00"
        starting_price = 10
        image = '/media/images/chevrolet.jpg'
        description = 'beli dong'
        agree = True
        price = 0
        status = 'Available'
        SellerModel.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            email=email,
            bank_name=bank_name,
            bank_account=bank_account,
            title=title,
            end_date=end_date,
            starting_price=starting_price,
            image=image,
            description=description,
            agree=agree,
            price=price,
            status=status
        )
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Sold out',content)

    def test_if_sort_by_form_valid(self):
        form = SortByForm(data={'sort_by': 'Newest'})
        self.assertTrue(form.is_valid())

    def test_if_sort_by_form_works_by_newest(self):
        test = 'Newest'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_oldest(self):
        test = 'Oldest'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_alphabetical(self):
        test = 'Alphabetical'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_lowest_price(self):
        test = 'Price (from lowest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_highest_price(self):
        test = 'Price (from highest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_nearest_end_date(self):
        test = 'End Date (from nearest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_farthest_end_date(self):
        test = 'End Date (from farthest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')