from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

from django.conf import settings
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('home.urls')),
    path('', include('description.urls')),
    path('SellerForm', include('seller.urls')),
	path('CustomerForm/', include('CustomerForm.urls')),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$',
        serve, {'document_root':
        settings.MEDIA_ROOT, }),
    ]
