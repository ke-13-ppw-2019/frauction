from django.shortcuts import render, redirect
from django.urls import reverse
from .models import SellerModel
from . import forms

def seller_create(request):
    if request.method == 'POST':
        form = forms.SellerForm(request.POST, request.FILES)
        if form.is_valid():
            nama = form.cleaned_data['name']
            title = form.cleaned_data['title']
            end_date = form.cleaned_data['end_date']
            form.save()
            return render(request, 'thanks.html', {
                'nama': nama,'title':title,'end_date':end_date,
                'navlink' : reverse('seller_create'),
                'sellorbuy' : 'Sell', })
    else:
        form = forms.SellerForm()
    return render(request, 'seller_create.html', {
        'form': form,
        'navlink' : reverse('home:home'),
        'sellorbuy' : 'Buy',})

