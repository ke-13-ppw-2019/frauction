from django.db import models

class SellerModel(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.PositiveIntegerField()
    email = models.EmailField()
    bank_name = models.CharField(max_length=50)
    bank_account = models.CharField(max_length=20)
    title = models.CharField(max_length=100)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()
    starting_price = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to="images")
    description = models.TextField()
    agree = models.BooleanField()
    
    price = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=15,default='Available')

    def __str__(self):
        return self.name