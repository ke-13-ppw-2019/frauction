from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import SellerModel
from .forms import SellerForm


# Create your tests here.
class Testing(TestCase):
    def test_apakah_url_seller_ada(self):
            response = Client().get('/SellerForm')
            self.assertEqual(response.status_code, 200)

    def test_apakah_jika_memasukkan_url_lain_maka_url_tidak_ada(self):
            response = Client().get('/notexist/')
            self.assertEqual(response.status_code, 404)

    def test_apakah_seller_html_ditampilkan(self):
            response = Client().get('/SellerForm')
            self.assertTemplateUsed(response, 'seller_create.html')

    def test_apakah_terdapat_fungsi_seller_create(self):
            found = resolve('/SellerForm')
            self.assertEqual(found.func, seller_create)

    def test_apakah_terdapat_tulisan_PleaseFillOutThisInformation(self):
            request = HttpRequest()
            response = seller_create(request)
            html_response = response.content.decode('utf8')
            self.assertIn('Please Fill Out This Information', html_response)

  



