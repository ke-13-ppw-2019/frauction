from django import forms
from .models import SellerModel

class DateInput(forms.DateInput):
    input_type='date'

class SellerForm(forms.ModelForm):
    class Meta:
        model = SellerModel
        fields = ['name', 'address', 'phone_number', 'email', 'bank_name',
                  'bank_account', 'title', 'end_date', 'starting_price', 'image', 'description', 'agree']
        widgets = {
            'end_date': DateInput(),
            'email' : forms.EmailInput(attrs={'type':'email'}),
            'phone_number' : forms.NumberInput(attrs={'type': 'number'}),
            'starting_price' : forms.NumberInput(attrs={'type': 'number'}),
            'agree' : forms.CheckboxInput(attrs={'type': 'checkbox'}),
        }
        labels = {
            'agree' : "I have agreed to the terms and conditions",
            'starting_price' : "Starting price"
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
