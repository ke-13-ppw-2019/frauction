from django.urls import path
from . import views

urlpatterns = [
    path('', views.seller_create, name='seller_create'),
]
