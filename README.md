# Frauction

[![pipeline status](https://gitlab.com/ke-13-ppw-2019/frauction/badges/master/pipeline.svg)](https://gitlab.com/ke-13-ppw-2019/frauction/commits/master)

A Django based web application to fulfill PPW assignment.

# Authors
Glenda Emanuella Sutanto ([glendaesutanto](https://gitlab.com/glendaesutanto))

Wulan Mantiri ([wulanmantiri_](https://gitlab.com/wulanmantiri_))

Vikih Fitrianih ([VikihFitrianih](https://gitlab.com/VikihFitrianih))

Amalia Hanisafitri  ([amaliahanisa](https://gitlab.com/amaliahanisa))

# About Frauction
Frauction is an online platform to buy and sell properties which follows auction procedures. Frauction guarantees accessibility, credibility, and comfortability for any transaction between buyers and sellers.

# Features
Frauction offers 4 features (following the order of authors) :

1. Home

    Home is first page of Frauction that displays many product options with their image, title, and price. Sort Feature is available for buyers to narrow and specify their needs based on pricing, time limit, and alphabet order.

2. Description

    Clicking one of the product options in Home will direct to Description Feature, which contains  details on the auction and payment along with item description and other additional information. Question and Answer (QnA) Feature is offered for transparency between buyers and sellers.

3. Customer Form

    Customers are required to fill the form if they wish to place bids on the products. The form consists of the amount to bid (with the current price set as minimum price) and the customer's biodata, such as full name, address, phone number, and email.

4. Seller Form

    The product options are obtained from the data filled by sellers in Seller Form, which is similar to Customer Form except for the form for bidding amount. Data such as seller's bank account and name are also mandatory to transfer the money obtained after the auction finishes. As for item information, sellers are required to provide the title, end date, starting price, description, and image.


# Heroku Link
[frauction.herokuapp.com](http://frauction.herokuapp.com/)